---

- name: Install root authorized key
  ansible.posix.authorized_key:
    user: "root"
    key: "{{ root_ssh_key }}"
  when: (root_ssh_key is defined) and (root_ssh_key)

- name: Create ops group
  ansible.builtin.group:
    name: "{{ opsgroup }}"
    state: present
# no need to define guid right now

# no need to define uid right now
# sudogroup is defined per OS
- name: Create ops user
  ansible.builtin.user:
    name: "{{ opsuser }}"
    group: "{{ opsgroup }}"
    state: present
    append: true
    groups: "{{ sudogroup }}"

- name: Install opsuser authorized key
  ansible.posix.authorized_key:
    user: "{{ opsuser }}"
    key: "{{ opsuser_ssh_key }}"
  when: (opsuser_ssh_key is defined) and (opsuser_ssh_key)
  ignore_errors: "{{ ansible_check_mode }}"

# does not handle "local" creation separately
- name: Create operators
  ansible.builtin.user:
    state: "{{ item.provision_state | default(present) }}"
    name: "{{ item.name }}"
    group: "{{ opsgroup }}"
    append: true
    groups: "{{ sudogroup }}"
    update_password: on_create
    # changemeplease
    password: $6$Q6UJ5WJK2Jkdi2jo$4qqkfbI.6A.Hf7JNb3EjEroZZE0td16qANHwkgvZtEiOfGlfldim5EKzxEBJ4RMSr9wzMroizwvw7sD5z1Mko0
  loop: "{{ user_list }}"
  register: user_creation_results
  ignore_errors: "{{ ansible_check_mode }}"


# this is a little tricky because a change might be related
# to the groups. most of the time this should operate correctly.
- name: Expire passwords on user creations
  ansible.builtin.command:
    cmd: passwd -e {{ item.item.name }}
  loop: "{{ user_creation_results.results }}"
  when: item.changed and ('present' in item.item.provision_state)

- name: Add user comments
  ansible.builtin.user:
    name: "{{ item.name }}"
    comment: "{{ item.comment }}"
  when: (item.comment is defined) and (item.comment)
  loop: "{{ user_list }}"
  # when: (item.item.comment is defined) and (item.item.comment)

- name: Ensure user shells
  ansible.builtin.user:
    name: "{{ item.name }}"
    shell: "{{ item.shell }}"
  when: (item.shell is defined) and (item.shell)
  loop: "{{ user_list }}"

- name: Ensure user homedirs
  ansible.builtin.user:
    name: "{{ item.name }}"
    move_home: true
    home: "{{ item.home }}"
  when: (item.home is defined) and (item.home)
  loop: "{{ user_list }}"

# we ignore errors during check mode because it is an automatic
# error to create authorized keys for non-existent users.
- name: Create operator sshkeys
  ansible.posix.authorized_key:
    user: "{{ item.0.name }}"
    key: "{{ item.1 }}"
  loop: "{{ user_list | subelements('ssh_authkey', skip_missing=True) }}"
  ignore_errors: "{{ ansible_check_mode }}"
