# General config notes
### variables
Variables specific to each operating system distribution are kept in
the vars directory, by name of distribution. These variables are parsed
by utilizing a stanza in the tasks/main.yml file which will use the
"first found" file by name, of a certain format.
### default user
There is actually a default user created by this role, because we are
expected to use this role to set up ops users, not create them manually.

If you see "user1" in your `check_mode` run, it's because you have not yet
set up your users dictionary. If you don't like "user1", it is suggested
that you copy over what's in the vars/main.yml file and override it with
your own values for operators.
