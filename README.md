Role Name
=========

Common Linux host configurations as code for hosts inside the MergeTB infrastructure. Should be generic enough for any host, though.
Also, see [README_notes.md](./README_notes.md)

Requirements
------------

No special requirements outside of ansible/python being functional.

Role Variables
--------------

TODO: fill this out

Dependencies
------------

N/A (SHould strive to keep this independent of other galaxy roles).

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: mergetb_common_hostconf, ntpd_package: sntp }

License
-------

BSD-3-Clause

Author Information
------------------

jdbarnes@isi.edu
